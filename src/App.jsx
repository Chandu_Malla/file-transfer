import React, { useState } from 'react';
import './App.css';

const data = {
  HTML: [true, false],
  CSS: [true, false],
  JAVASCRIPT: [true, false],
  DOM: [true, false],
  REACT: [true, false],
  VUE: [true, false],
};

const App = () => {
  const [checkedItems, setCheckedItems] = useState(data);
  const [leftItems, setLeftItems] = useState(Object.keys(checkedItems));
  const [rightItems, setRightItems] = useState([]);

  const moveCheckedToRight = () => {
    const updatedLeftItems = leftItems.filter((key) => !checkedItems[key][0]);
    const updatedRightItems = rightItems.concat(leftItems.filter((key) => checkedItems[key][0]));

    setLeftItems(updatedLeftItems);
    setRightItems(updatedRightItems);
  };

  const moveAllToRight = () => {
    setRightItems([...rightItems, ...leftItems]);
    setLeftItems([]);
  };

  const moveCheckedToLeft = () => {
    const updatedRightItems = rightItems.filter((key) => !checkedItems[key][1]);
    const updatedLeftItems = leftItems.concat(rightItems.filter((key) => checkedItems[key][1]));

    setRightItems(updatedRightItems);
    setLeftItems(updatedLeftItems);
  };

  const moveAllToLeft = () => {
    setLeftItems([...leftItems, ...rightItems]);
    setRightItems([]);
  };

  return (
    <div className="container">
      <div className="inner1">
        {leftItems.map((key) => (
          <div key={key}>
            <input
              type="checkbox"
              checked={checkedItems[key][0]}
              onChange={() => {
                const updatedItems = { ...checkedItems };
                updatedItems[key] = [!checkedItems[key][0], checkedItems[key][1]];
                setCheckedItems(updatedItems);
              }}
            />
            <label>{key}</label>
          </div>
        ))}
      </div>

      <div>
        <button id="single-greater" onClick={moveCheckedToRight}>Move Checked To Right</button>
        <button id="double-greater" onClick={moveAllToRight}>Move All To Right</button>
        <button id="single-less" onClick={moveCheckedToLeft}>Move Checked To Left</button>
        <button id="double-less" onClick={moveAllToLeft}>Move All To Left</button>
      </div>

      <div className="inner2">
        {rightItems.map((key) => (
          <div key={key}>
            <input
              type="checkbox"
              checked={checkedItems[key][1]}
              onChange={() => {
                const updatedItems = { ...checkedItems };
                updatedItems[key] = [checkedItems[key][0], !checkedItems[key][1]];
                setCheckedItems(updatedItems);
              }}
            />
            <label>{key}</label>
          </div>
        ))}
      </div>
    </div>
  );
};

export default App;
